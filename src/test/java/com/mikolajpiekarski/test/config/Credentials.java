package com.mikolajpiekarski.test.config;

/**
 * Class for mapping credentials from a json file
 */
public class Credentials {
    private String username;
    private String password;

    // Getters
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
