package com.mikolajpiekarski.test.config;

/**
 * Class for mapping the driver config from a json file
 * All config variables are in lowercase
 */
public class DriverConfig {
    private String browser;
    private int implicitWait;
    private String startUrl;

    // Getters
    public String getBrowser() {
        return browser;
    }

    public int getImplicitWait() {
        return implicitWait;
    }

    public String getStartUrl() {
        return startUrl;
    }
}
