package com.mikolajpiekarski.pages.sales.items.commonelements;

import com.mikolajpiekarski.pages.base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class representing a details card/tab
 */
public class DetailsCard extends PageElement {
    public DetailsCard(WebDriver driver) {
        super(driver.findElement(selfLocator), driver);
    }

    // Locators
    private static final By selfLocator = By.xpath(
            "//div[contains(@class, 'oneContent active')]" +
                    "//*[@aria-labelledby='detailTab__item']");
    private final By outputFieldsLocator = By.xpath(
            ".//records-record-layout-item/div/div[not(contains(@class, 'empty-label'))]");
    private final By outputFieldLabel = By.xpath(
            ".//span[@class='test-id__field-label']");
    private final By outputFieldData = By.xpath(
            ".//*[@slot='outputField']");
    private final By outputFieldForceLookupData = By.xpath(".//span[@force-lookup_lookup]");

    // Methods

    /**
     * Gets the data displayed in the detail fields
     * @return HashMap with keys as field labels in lowercase and values as data
     */
    public HashMap<String, String> getDetails(){
        List<WebElement> outputFields = findElements(outputFieldsLocator);
        HashMap<String, String> data = new HashMap<>();
        for (WebElement outputField :
                outputFields) {
            Map.Entry<String, String> dataEntry = getDataFromOutputField(outputField);
            data.put(dataEntry.getKey(), dataEntry.getValue());
        }

        return data;
    }

    /**
     * Extracts data from the specified output field
     * @param outputField Output field
     * @return Map entry with key as field label(in lowercase) and value as field data
     */
    private Map.Entry<String, String> getDataFromOutputField(WebElement outputField){
        String label = outputField.findElement(outputFieldLabel).getText();
        WebElement dataElement = outputField.findElement(outputFieldData);

        String data;
        if (dataElement.getTagName().equals("force-lookup")){
            // force-lookup elements have assistive text inside which pollutes data, so it searches for direct data
            data = dataElement.findElement(outputFieldForceLookupData).getText();
        } else {
            data = dataElement.getText();
        }

        data = cleanOutputFieldData(data);

        return Map.entry(label.toLowerCase(), data);
    }

    /**
     * Cleans the data from redundant characters
     * @param data Data to be cleaned
     * @return Cleaned data
     */
    private String cleanOutputFieldData(String data) {
        data = data.replace(",", ""); // Commas are not needed for comparison
        data = data.replace("$", ""); // Dollar hampers the comparison process

        return data;
    }
}
