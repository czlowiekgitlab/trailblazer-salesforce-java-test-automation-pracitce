package com.mikolajpiekarski.pages.sales.items.account;

import com.mikolajpiekarski.pages.sales.base.SalesBasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

/**
 * Class representing a page with a list of existing accounts
 */
public class SalesAccountListPage extends SalesBasePage {

    public SalesAccountListPage(WebDriver driver) {
        super(driver, "Accounts | Salesforce", true);
    }

    // Locators
    private final By dropdownSelectedListView = By.cssSelector("div.oneContent.active .selectedListView");
    private final By dropdownItemAllAccountsListViewLocator = By.xpath("//span[text()='All Accounts']");
    private final By loadingSpinnerLocator = By.cssSelector("div.slds-spinner_container:not(.slds-hide):not(.hidden)");

    // Dynamic locators
    private By accountLocator(String accountName){
        return By.cssSelector("a[title='" + accountName + "']");
    }

    // Navigation methods
    /**
     * Clicks the dropdown with list type selection
     */
    public void clickDropdownSelectedListView(){
        findElement(dropdownSelectedListView).click();
    }

    /**
     * Selects the list displaying all existing accounts
     */
    public void clickDropdownItemAllAccountsListView(){
        findElement(dropdownItemAllAccountsListViewLocator).click();
        waitUntil(ExpectedConditions.invisibilityOf(findElement(loadingSpinnerLocator)), Duration.ofSeconds(10));
    }

    /**
     * Selects the account with a specified name
     * @param accountName Name of the account to be selected
     * @return Specified account page
     */
    public SalesAccountPage clickAccount(String accountName){
        try{
            findElement(accountLocator(accountName)).click();
        } catch (NotFoundException e){
            throw new RuntimeException("Account not found: " + accountName, e);
        }

        return new SalesAccountPage(getDriver(), accountName);
    }
}
