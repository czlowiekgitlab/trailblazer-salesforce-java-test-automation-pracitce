package com.mikolajpiekarski.pages.sales.items.commonelements.modalitemoperation.base;

import com.mikolajpiekarski.pages.base.PageElement;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ModalItemOperation extends PageElement {
    private final String itemName;
    private final String operationType;

    public ModalItemOperation(WebDriver driver, String operationType, String itemName) {
        super(driver.findElement(selfLocator), driver);

        String headerText = findElement(headerLocator).getText();
        if (!headerText.startsWith(operationType)
        || !headerText.endsWith(itemName))
            throw new RuntimeException("Wrong modal window accessed: " +
                    "expected " + operationType + " " + itemName + ", actual " + headerText);

        this.itemName = itemName;
        this.operationType = operationType;
    }

    // Locators
    private static final By selfLocator = By.cssSelector("div.isModal.active");
    private final By headerLocator = By.tagName("h2");
    private final By inputLabelsLocator = By.cssSelector("label.slds-form-element__label");
    private final By saveButtonLocator = By.cssSelector("button[name='SaveEdit']");
    private final By alertMessageLocator = By.cssSelector(".toastMessage");

    // Dynamic locators
    private By getInputFieldLocator(String inputId){
        return By.xpath(".//*[@id='" + inputId + "']");
    }

    private By getComboboxDropdownItemLocator(String value) {
        return By.xpath("../..//span[text()='" + value + "']");
    }

    private static By getComboboxLookupItemLocator(String value) {
        return By.xpath("../..//*[@title='" + value + "']/strong");
    }

    /**
     * Gets specified dropdown item
     * @param comboboxDropdown Dropdown to choose items from
     * @param value Value of the item
     * @return WebElement of the chosen item
     */
    private WebElement getComboboxDropdownItem(WebElement comboboxDropdown, String value){
        comboboxDropdown.click();
        return comboboxDropdown.findElement(getComboboxDropdownItemLocator(value));
    }

    /**
     * Gets specified combobox lookup item
     * @param comboboxLookup Combobox lookup to choose items from
     * @param value Value of the item
     * @return WebElement of the chosen item
     */
    private WebElement getComboboxLookupItem(WebElement comboboxLookup, String value){
        comboboxLookup.sendKeys(value);
        return comboboxLookup.findElement(getComboboxLookupItemLocator(value));
    }

    // Methods
    /**
     * Inputs values into the modal window form elements
     * @param values HashMap with keys as data field names(corresponding to form input labels) and values as data values
     */
    public void inputValuesToForm(HashMap<String, String> values){
        HashMap<String, WebElement> formInputElements = getFormInputElements();
        for (Map.Entry<String, String> value :
                values.entrySet()) {
            fillInputElement(formInputElements.get(value.getKey().toLowerCase()), value.getValue());
        }

    }

    /**
     * Clicks the save button and waits for the modal to close
     */
    public void clickSaveButton(){
        findElement(saveButtonLocator).click();
        // Wait for modal window to be closed
        waitUntil(ExpectedConditions.stalenessOf(getSelf()), Duration.ofSeconds(10));
    }

    /**
     * Fills the element with a certain value. Handles input element type.
     * @param inputElement Input element
     * @param value Value to be put in the element
     */
    private void fillInputElement(WebElement inputElement, String value) {
        if (inputElement == null)
            throw new NoSuchElementException("No form input for value: " + value);

        switch (inputElement.getAriaRole()){
            case "textbox":
                inputTextboxValue(inputElement, value);
                break;
            case "combobox":
                if (inputElement.getTagName().equals("input"))
                    inputComboboxLookupValue(inputElement, value);
                else
                    inputComboboxDropdownValue(inputElement, value);
                break;
            default:
                throw new UnsupportedOperationException(
                        "Unknown input element in " + itemName + " " + operationType + " window");
        }
    }

    /**
     * Chooses value from a combobox dropdown element
     * @param inputElement Combobox dropdown element
     * @param value Value to be chosen
     */
    private void inputComboboxDropdownValue(WebElement inputElement, String value) {
        // None options in dropdowns have dashes
        try {
            getComboboxDropdownItem(inputElement, value).click();
        } catch (NoSuchElementException e){
            throw new RuntimeException(
                    "No value found in dropdown " + inputElement.getAccessibleName() + ": " + value, e);
        }
    }

    /**
     * Chooses value from a combobox lookup element
     * @param inputElement Combobox lookup element
     * @param value Value to be searched and chosen
     */
    private void inputComboboxLookupValue(WebElement inputElement, String value) {
        inputElement.sendKeys(Keys.BACK_SPACE); // Removes previous value
        if (value.isBlank())
            return;

        try {
            getComboboxLookupItem(inputElement, value).click();
        } catch (NoSuchElementException e){
            throw new RuntimeException(
                    "No value found in combobox lookup " + inputElement.getAccessibleName() + ": " + value, e);
        }
    }

    /**
     * Inputs value into a textbox
     * @param inputElement Textbox element
     * @param value Value to be input
     */
    private void inputTextboxValue(WebElement inputElement, String value) {
        inputElement.clear();
        inputElement.sendKeys(value);
    }

    /**
     * Gets all available input elements from the modal window
     * @return HashMap with input labels as keys and values as input form elements
     */
    private HashMap<String, WebElement> getFormInputElements(){
        HashMap<String, WebElement> inputElements = new HashMap<>();
        List<WebElement> inputLabels = findElements(inputLabelsLocator);

        if (inputLabels.isEmpty())
            throw new RuntimeException("No input elements found in "  + itemName + " " + operationType + " window");

        for (WebElement inputLabel :
                inputLabels) {
            String labelText = inputLabel.getText().toLowerCase().replaceFirst("\\*", "");
            String inputId = inputLabel.getAttribute("for"); // Gets input element id from the corresponding label
            WebElement inputElement = findElement(getInputFieldLocator(inputId));
            inputElements.put(labelText, inputElement);
        }

        return inputElements;
    }
}
