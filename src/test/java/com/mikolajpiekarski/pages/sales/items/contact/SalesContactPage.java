package com.mikolajpiekarski.pages.sales.items.contact;

import com.mikolajpiekarski.pages.sales.base.SalesBasePage;
import com.mikolajpiekarski.pages.sales.items.account.SalesAccountPage;
import com.mikolajpiekarski.pages.sales.items.commonelements.DetailsCard;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Class representing a contact item page
 */
public class SalesContactPage extends SalesBasePage {
    private final String relatedAccount;

    public SalesContactPage(WebDriver driver, String contactName, String relatedAccount) {
        super(driver, contactName + " | Salesforce");
        this.relatedAccount = relatedAccount;
    }

    // Locators
    private final By detailsTabLocator = By.xpath(
            "//div[contains(@class, 'oneContent active')]//li[@title='Details']/a");

    private final By moreActionsButtonLocator = By.xpath("//div[contains(@class, 'oneContent active')]" +
            "//div[contains(@class, 'actionsContainer')]//lightning-button-menu");
    private final By deleteButtonLocator = By.xpath("//div[contains(@class, 'oneContent active')]//*[@title='Delete']");
    private final By deleteButtonModalLocator = By.xpath("//div[@role='dialog']//button[@title='Delete']");

    // Navigation methods

    /**
     * Clicks the details tab
     * @return Details card object
     */
    public DetailsCard clickDetailsTab() {
        findElement(detailsTabLocator).click();
        return new DetailsCard(getDriver());
    }

    /**
     * Clicks an arrow button and opens a list with more item actions
     */
    public void clickMoreActionsArrowButton(){
        findElement(moreActionsButtonLocator).click();
    }

    /**
     * Clicks the delete item action. Requires more actions button to be clicked before.
     */
    public void clickDeleteButton(){
        findElement(deleteButtonLocator).click();
    }

    /**
     * Clicks the delete button in a modal window
     * @return Related account item page
     */
    public SalesAccountPage clickDeleteModalButton(){
        findElement(deleteButtonModalLocator).click();
        return new SalesAccountPage(getDriver(), relatedAccount);
    }

}
