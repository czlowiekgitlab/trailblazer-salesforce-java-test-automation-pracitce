package com.mikolajpiekarski.pages.sales.base.commonelements;

import com.mikolajpiekarski.pages.LogInPage;
import com.mikolajpiekarski.pages.base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

/**
 * Class representing a page header
 */
public class Header extends PageElement {

    public Header(WebDriver driver) {
        super(driver.findElement(selfLocator), driver);
    }

    // Locators
    private static final By selfLocator = By.id("oneHeader");
    private final By searchLocator = By.cssSelector(".search-button");
    private final By userProfileButtonLocator = By.cssSelector(".branding-userProfile-button");

    // Navigation methods
    /**
     * Clicks a search bar in the header
     * @return Global search dialog window
     */
    public SearchDialog clickSearch(){
        findElement(searchLocator).click();
        return new SearchDialog(getDriver());
    }

    /**
     * Clicks the user profile button in the top right corner
     * @return Dialog with user operations and settings
     */
    public UserProfileDialog clickUserProfile(){
        findElement(userProfileButtonLocator).click();
        return new UserProfileDialog(getDriver());
    }

    // Inner classes

    /**
     * Represents a search dialog which pops up after clicking the search bar
     */
    public class SearchDialog extends PageElement{

        public SearchDialog(WebDriver driver) {
            super(driver.findElement(selfLocator), driver);
        }

        // Locators
        private static final By selfLocator = By.cssSelector("div.forceSearchAssistantDialog");
        private final By searchInputLocator = By.cssSelector("input[type=search]");

        // Dynamic locators
        private By getResultElementLocator(String searchQuery){
            return By.xpath("//search_dialog-instant-result-item//span[.='" + searchQuery + "']");
        }

        // Navigation methods
        /**
         * Inputs a search phrase into a search bar
         * @param searchQuery Search phrase to input
         */
        public void searchFor(String searchQuery){
            findElement(searchInputLocator).sendKeys(searchQuery);
        }

        /**
         * Closes the search dialog
         */
        public void exitSearchDialog(){
            findElement(searchInputLocator).sendKeys(Keys.ESCAPE);
        }

        /**
         * Checks if search phrase has been found
         * @param result Search phrase that has been passed to the search bar
         * @return True if found, false if not
         */
        public boolean checkResult(String result){
            try{
                waitUntil(ExpectedConditions.presenceOfAllElementsLocatedBy(getResultElementLocator(result)),
                        Duration.ofSeconds(5));
            }catch (TimeoutException e){
                return false;
            }
            return true;
        }
    }

    /**
     * Class representing a dialog window with current user
     */
    public class UserProfileDialog extends PageElement{

        public UserProfileDialog(WebDriver driver) {
            super(driver.findElement(selfLocator), driver);
        }

        // Locators
        private final static By selfLocator = By.cssSelector(".oneUserProfileCard");
        private final By logoutLocator = By.cssSelector(".logout");

        // Navigation methods
        /**
         * Clicks an option to logout of the profile
         * @return Login page object
         */
        public LogInPage clickLogout(){
            findElement(logoutLocator).click();
            return new LogInPage(getDriver());
        }
    }
}
