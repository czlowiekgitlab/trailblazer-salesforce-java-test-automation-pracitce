package com.mikolajpiekarski.pages;

import com.mikolajpiekarski.pages.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogInPage extends BasePage {
    public LogInPage(WebDriver driver) {
        super(driver, "Login | Salesforce");
    }

    // Locators
    private final By usernameLocator = By.id("username");
    private final By passwordLocator = By.id("password");
    private final By buttonLoginLocator = By.id("Login");

    // Navigation methods

    /**
     * Fills username field
     * @param username Username
     */
    public void fillUsername(String username){
        findElement(usernameLocator).sendKeys(username);
    }

    /**
     * Fills password field
     * @param password Password
     */
    public void fillPassword(String password){
        findElement(passwordLocator).sendKeys(password);
    }

    /**
     * Clicks log in button
     * @return Salesforce home page object
     */
    public HomePage clickLogIn(){
        findElement(buttonLoginLocator).click();
        return new HomePage(getDriver());
    }
}
