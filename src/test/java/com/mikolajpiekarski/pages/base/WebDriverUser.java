package com.mikolajpiekarski.pages.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

/**
 * Abstract class for elements which use a WebDriver
 */
public abstract class WebDriverUser {
    private final WebDriver driver;
    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverUser(WebDriver driver) {
        this.driver = driver;
    }

    // Methods
    /**
     * Helper method for increased readability
     * @param by Locator
     * @return Found WebElement
     */
    public WebElement findElement(By by){
        return this.driver.findElement(by);
    }

    /**
     * Helper method for increased readability
     * @param by Locator
     * @return Found WebElement list
     */
    public List<WebElement> findElements(By by){
        return this.driver.findElements(by);
    }

    /**
     * Helper method to increase readability
     * @param expectedCondition Selenium expected condition from ExpectedConditions class
     * @param timeout Timeout duration
     */
    public void waitUntil(ExpectedCondition<?> expectedCondition, Duration timeout) {
        WebDriverWait wait = new WebDriverWait(this.driver, timeout);
        wait.until(expectedCondition);
    }

    /**
     * Helper method to increase readability
     * @param expectedCondition Selenium expected condition from ExpectedConditions class
     * @param timeout Timeout duration
     */
    public void waitUntilNot(ExpectedCondition<?> expectedCondition, Duration timeout) {
        WebDriverWait wait = new WebDriverWait(this.driver, timeout);
        wait.until(ExpectedConditions.not(expectedCondition));
    }
}
