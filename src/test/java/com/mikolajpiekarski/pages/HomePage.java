package com.mikolajpiekarski.pages;

import com.mikolajpiekarski.pages.base.BasePage;
import com.mikolajpiekarski.pages.sales.SalesHomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver, "Home | Salesforce");
    }

    // Locators
    private final By buttonNavigationLocator = By.cssSelector("div[role='navigation']");
    private final By navigationSalesLocator = By.xpath("//p[text()='Sales']");

    // Navigation methods
    /**
     * Clicks the app navigation button
     */
    public void clickAppNavigationButton(){
        findElement(buttonNavigationLocator).click();
    }

    /**
     * Clicks the sales app in app navigation
     * @return Sales home page object
     */
    public SalesHomePage clickNavigationSalesApp(){
        findElement(navigationSalesLocator).click();
        return new SalesHomePage(getDriver());
    }
}
